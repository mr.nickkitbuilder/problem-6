import copy as c
i = 0
summ = 0
inData = 5
treshold = inData * inData
while True:
    currentLen = len(bin(i)[2:])
    current = bin(i)[2:]
    units = 0
    if currentLen > treshold:
        break
    if current[0] == '0' or current[currentLen-1] == '0':
        i+=1
        continue
    for j in range(currentLen):
        if current[j] == '1':
            units+=1
    if units != inData:
        i += 1
        continue
    jSum = 0
    result = True
    print(current)
    for j in range(currentLen):
        if current[j] == '0':
            jSum -= 1
        if current[j] == '1':
            jSum += 1
        if jSum < 0:
            result = False
            break

    if result == True:
        summ += 1
    i+=1
print(summ)

