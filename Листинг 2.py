import copy as c

class Node:
   leftNode = None
   rightNode = None
   weight = 0

def getChildAtRight(node, num):
   for i in range(num):
       node = node.rightNode
   return node

def treeBuilder(n):
  mainNode  = Node()
  mainNode.weight = 1
  currentLayer = [mainNode]
  newLayer = []
  for i in range(n):
    for j in range(len(currentLayer)):
      currentWeight = currentLayer[j].weight
      currentLayer[j].leftNode = Node()
      currentLayer[j].leftNode.weight = currentWeight + 1
      newLayer.append(currentLayer[j].leftNode)
      if currentWeight > 0:
        for k in range(currentWeight):
          currentNode = getChildAtRight(currentLayer[j],k)
          currentNode.rightNode = Node()
          currentNode.rightNode.weight = currentNode.weight - 1
          currentNode.rightNode.leftNode = Node()
          currentNode.rightNode.leftNode.weight = currentNode.rightNode.weight + 1
          newLayer.append(currentNode.rightNode.leftNode)

    currentLayer = c.deepcopy(newLayer)
    newLayer = []
  return len(currentLayer)

print(treeBuilder(3))




